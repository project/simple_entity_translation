<?php

/**
 * @file
 * Page callbacks for the simple_entity_translation.
 */

/**
 * Page callback: Displays a list of a entity's translations.
 *
 * @param $entity
 *   An entity object.
 *
 * @return
 *   A render array for a page containing a list of content.
 *
 * @see translation_menu()
 */
function _simple_entity_translation_overview($entity) {
  include_once DRUPAL_ROOT . '/includes/language.inc';
  $entity_type = $entity->base_type;

  // Get all related entity keys / properties.
  $info = entity_get_info($entity_type);
  $id_key = $info['entity keys']['id'];
  $label_key = $info['entity keys']['label'];
  $bundle_key = (isset($info['bundle keys']['bundle'])) ? $info['bundle keys']['bundle'] : NULL;
  $bundle = NULL;
  if (!empty($bundle_key)) {
    $bundle = $entity->$bundle_key;
  }
  $access = $info['access callback'];
  $if_status = (isset($info['base table field types']['status'])) ? TRUE : FALSE;

  if (!empty($entity->translation_id)) {
    // Already part of a set, grab that set.
    $tid = $entity->translation_id;
    $translations = _simple_entity_translation_get_translations($entity_type, $tid);
  }
  else {
    // We have no translation source player_id, this could be a new set, emulate that.
    $tid = $entity->$id_key;
    $translations = array(entity_language($entity_type, $entity) => $entity);
  }

  $type = variable_get('translation_language_type', LANGUAGE_TYPE_INTERFACE);

  $header = array();
  $header[] = t('Title');
  $header[] = t('Language');
  $header[] = t('Status');
  $header[] = t('Operations');

  foreach (language_list() as $langcode => $language) {
    $options = array();
    $language_name = $language->name;
    $path_info = _simple_entity_translation_get_base_path($entity_type, $info);
    $path = $path_info['base_path'];
    if (isset($translations[$langcode])) {
      $result = entity_load($entity_type, array($translations[$langcode]->$id_key));
      $translation = reset($result);
      $id = $translations[$langcode]->$id_key;
      $links = language_negotiation_get_switch_links($type, $path);
      $title = empty($links->links[$langcode]['href']) ? l($translation->$label_key, $path) : l($translation->$label_key, $links->links[$langcode]['href'], $links->links[$langcode]);
      if ($access('update', $translation)) {
        $text = t('edit');
        $edit = str_replace($path_info['wildcard'], $id, $path_info['base_pattern']);
        $links = language_negotiation_get_switch_links($type, $edit);
        $options[] = empty($links->links[$langcode]['href']) ? l($text, $edit) : l($text, $links->links[$langcode]['href'], $links->links[$langcode]);
      }
      $status = '';
      if ($if_status) {
        $status = $translation->status ? t('Active') : t('Not active');
        $status .= (isset($translation->translate)) ? ' - <span class="marker">' . t('outdated') . '</span>' : '';
      }

      if ($translation->$id_key == $tid) {
        $language_name = t('<strong>@language_name</strong> (source)', array('@language_name' => $language_name));
      }
    }
    else {
      $title = t('n/a');
      if (user_access('translate entities')) {
        $text = t('add translation');
        $path = $path . "add/$bundle";
        if (!empty($bundle)) {
          $path .= $path . "/$bundle";
        }
        $links = language_negotiation_get_switch_links($type, $path);
        $query = array('query' => array('translation' => $tid, 'target' => $langcode));
        $options[] = empty($links->links[$langcode]['href']) ? l($text, $path, $query) : l($text, $links->links[$langcode]['href'], array_merge_recursive($links->links[$langcode], $query));
      }
      $status = t('Not translated');
    }
    $rows[] = array($title, $language_name, $status, implode(" | ", $options));
  }

  drupal_set_title(t('Translations of %title', array('%title' => $entity->$label_key)), PASS_THROUGH);

  $build['translation_node_overview'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}
