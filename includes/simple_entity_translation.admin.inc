<?php

/**
 * @file
 * Admin section for the simple_entity_translation.
 */

/**
 * Builder function for the entity translation settings form.
 */
function simple_entity_translation_admin_form($form, $form_state) {
  $entities = entity_get_info();

  // Disable by default for core entities.
  unset($entities['node']);
  unset($entities['comment']);
  unset($entities['user']);
  unset($entities['taxonomy_vocabulary']);
  unset($entities['taxonomy_term']);
  unset($entities['file']);

  if (module_exists('bootstrap_tour')) {
    unset($entities['bootstrap_tour_step']);
  }

  if (module_exists('field_collection')) {
    unset($entities['field_collection_item']);
  }
  
  if (empty($entities)) {
    $form[] = array(
      '#markup' => t('There is no available entities.'),
    );
    return $form;
  }

  foreach ($entities as $entity_type => $info) {
    $options[$entity_type] = !empty($info['label']) ? t($info['label']) : $entity_type;
  }
  $enabled_types = array_filter(variable_get('simple_entity_translation_enable_types', array()));
  $form['enabled'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translatable entity types'),
    '#description' => t('Select which entities can be translated.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['enabled']['simple_entity_translation_enable_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $enabled_types,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );
  // We needs to add entity keys, schemas, etc.
  $form['#submit'][] = 'simple_entity_translation_admin_form_submit';
  return $form;
}

/**
 * Submit handler for the simple entity translation settings form.
 */
function simple_entity_translation_admin_form_submit($form, $form_state) {
  $enabled_types = $form_state['values']['simple_entity_translation_enable_types'];
  $fields = _simple_entity_translation_fields();
  $entities = entity_get_info();
  foreach ($enabled_types as $type => $value) {
    $base_table = $entities[$type]['base table'];
    $revision_table = (isset($entities[$type]['revision table'])) ? $entities[$type]['revision table'] : '';
    $module = (isset($entities[$type]['module'])) ? $entities[$type]['module'] : $type;
    if (!empty($value)) {
      _simple_entity_translation_update_fields($base_table, $fields);
      _simple_entity_translation_update_fields($revision_table, $fields);
    }
    else {
      unset($enabled_types[$type]);
      _simple_entity_translation_update_fields($base_table, $fields, 'remove', $module);
      _simple_entity_translation_update_fields($revision_table, $fields, 'remove', $module);
    }
  }
  variable_set('simple_entity_translation_enable_types', $enabled_types);
  // We need to rebuild a lot of hooks: entity, schema, menu, etc.
  drupal_flush_all_caches();
}

/**
 * Helper to add / remove field to entity base / revision tables.
 */
function _simple_entity_translation_update_fields($table_name, $fields, $action = 'add', $module = '') {
  if (empty($table_name)) {
    return;
  }
  foreach ($fields as $field => $spec) {
    if ($action == 'add') {
      if (!db_field_exists($table_name, $field)) {
        db_add_field($table_name, $field, $spec);
        $params = array('!field' => $field, '!table' => $table_name);
        drupal_set_message(t('!field field was added to the !table table.', $params));
      }
    }
    if ($action == 'remove') {
      if (db_field_exists($table_name, $field)) {
        $table = drupal_get_schema_unprocessed($module, $table_name);
        if (!isset($table['fields'][$field])) {
          db_drop_field($table_name, $field);
          $params = array('!field' => $field, '!table' => $table_name);
          drupal_set_message(t('!field field was removed from the !table table.', $params));
        }
      }
    }
  }
}
